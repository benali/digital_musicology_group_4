# Did Vivaldi really "write the same concerto 500 times"?

## Table of contents
* [Project Members](#project-members)
* [Introduction](#introduction)
* [Instructions](#instructions)

## Project members:

+ Ben Ali, Maximilian
+ Donnet, Anne
+ Laini, Eva

## Introduction:

There is a saying, usually (and most likely incorrectly) attributed to Stravinsky and which most musician heard at least once, which claims that

"[Vivaldi] did not compose 500 concerti but wrote the same one 500 times."

In this project, we explore the similarities between pieces of the same composer, and whether their preponderance in Vivaldi's concerti compared to other composers would be such as to justify – or at least partly justify – the aforementioned saying. Moreover, we analyse whether there is a greater similarity between Vivaldi's concerti compared to his other works, or if he simply had a recognisable style.

In other words, we mesure the similarity of Vivaldi's concerti, of his other works, of others composers' concerti, and compare those mesurement against each other. These similarities are computed based on 3 different features categories:

- The relation between movements inside a piece : keys and tempo structure
- The structure of the pieces in relation to the solo/tutti separation
- Pitches and rythmic patterns

In order to compare pieces, we work with two different types of information:

- Metadata, for comparing the keys and tempo structure
- MIDI files, for exploring the solo/tutti separation and the pitches/rythmic patterns

TODO: Add summary of results

## Instructions:

This repository contains our semester project for the EPFL Digital Musicology class. Below you will find all necessary informations to use our code and reproduce our results.

### 1. 	Clone repository

First clone the gitlab repository on your own machine. The repository consists of :

- `milestone3.ipynb` notebook containing the project overview, data exploration and first results
- `my_utils.ipynb` notebook containing necessary imports and functions

- `scrap_links.py` script for scraping www.imslp.com for the musical pieces’ page links 
- `scrap_meta.py` script to get the pieces metadata from the scraped links 
- `geckodrivers` folder containing geckodrivers used for the scaping of links in www.imslp.com

### 2.	Get the project data from Dropbox

From https://www.dropbox.com/home/Musicology%20Project%20Data, download the data used in the project and put the data folder in the same folder as the notebook. 

- Folder containing the MIDI files used in the project
- Tsv files containing the links scraped from imslp.com and the metadata obtained from the links
- Folder containing results of pattern recognition analysis

### 3.	_Optional:_ Run scraper for imslp.com links and metadata

The metadata used in the project can be found in the Dropbox above, but if needed, use the scraper to get the links and associated metadata of the pieces from imslp.com:

First get all links from imslp.com by running the following commands in the cloned repository:

`python3 scrap_links.py data/links.tsv` 

TODO : add import lxml

_Note :_ the file will be created in the existing folder `data`. The links will be appened to existing file if not empty 

_Attention :_  the code only works with firefox installed - Chrome can be used by adding it as arg IF Firefox is installed on machine

Then, get all metadata from the scraped links :

`python3 scrap_meta.py data/links.tsv data/metadata.tsv True` 

TODO: remove arg True, False was if links were taken from a complete csv file

TODO:  add `with open(output_file, 'a+', encoding = "utf-8") as of:` for windows users

_Note:_ for Windows 10, use `py` instead of `python3`

### 4.	Run notebook

Finally, the notebook `milestone3.ipynb` can be run and the analysis of the MIDI files and metadata will be done.

_Note :_ Musescore 3 is required, default assumes mac users installed it through brew.

_Attention:_  `pyinterval` does not compile on Windows because of `crlibm`, so the pattern analysis part does not run for Windows users
