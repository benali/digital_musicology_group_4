# import the required libraries
import requests
from bs4 import BeautifulSoup
import sys
import csv
import re

NaN = float('NaN')
INPUT_FILE_COLUMN = 6
STATUS_COLUMN = 3
STATUS_OK = ["ok"]

def true_or_false(s):
    if(s.lower()=='true'):
        return True
    elif(s.lower()=='false'):
        return False
    else:
        print(f"Invalid argument {s}")
        return

def add_info(base_string, new_string):
    return base_string + "\t" + new_string

def get_td_from_span(table, regex):
    span = table.find('span', text=re.compile(regex))
    if(span is not None and span.parent is not None and span.parent.find_next_sibling("td")):
        return span.parent.find_next_sibling("td").text
    else:
        return NaN

def get_td_from_th(table, regex):
    th = table.find('th', text=re.compile(regex))
    if(th is not None):
        return th.find_next_sibling("td").text
    else:
        return NaN

def format_movements(movements):
    if(movements != movements):
        return NaN, NaN

    nb_of_movements = movements[0]

    if(movements.count('\n') < 2):
        return nb_of_movements, NaN

    movements = movements.split('\n')[1:-2]
    movements = [re.sub(r'I*\. ', '', mv) for mv in movements]

    mv_string = movements[0]
    for mv in movements[1:]:
       mv_string = mv_string + "; " + mv

    return nb_of_movements, mv_string

def format_instru(instrumentation):
    instrumentation = re.sub(r'solo:', '', instrumentation)
    instrumentation = re.sub(r'orchestra:', ',', instrumentation)
    return instrumentation.replace(',', ';').replace('\n', '')#.replace(' ', '').strip()

def get_metadata_from_soup(soup, output_file, csv_writer):
    if(soup is not None):
        for div in soup.find_all('div', class_="wi_body"):
            for table in div.find_all('table'):

                id_ = get_td_from_span(table, r'Opus')
                if(id_ == id_):
                    opus_regex = re.compile(r'RV|BWV|TWV')
                    if(opus_regex.search(id_)):
                        split_id = id_.split()
                        id_ = split_id[0] + split_id[1]
                    else:
                        id_ = NaN

                composer = get_td_from_th(table, r'Composer')
                if(composer == composer):
                    split_composer = composer.split(',')
                    composer = split_composer[0].strip()

                work_title = get_td_from_th(table, r'Work')
                if(work_title == work_title):
                    work_title = work_title.strip()

                keys = get_td_from_th(table, r'Key')
                if(keys == keys):
                    keys = keys.strip()


                nb_of_movements, movements = format_movements(get_td_from_span(table, r'Movem'))

                instrumentation = format_instru(get_td_from_th(table, r'Instru'))

                information_row = [id_, composer, work_title, keys, nb_of_movements, movements, instrumentation]
                csv_writer.writerow(information_row)

def get_soup_from_link(link):
    response = requests.get(link)

    # Store the webpage contents
    webpage = response.content

    if(response.status_code != 200):
        return None

    # Create a BeautifulSoup object out of the webpage content
    return BeautifulSoup(webpage, "html.parser")

# Main
print(str(sys.argv))

file_links = sys.argv[1]
output_file = sys.argv[2]
only_links = true_or_false(sys.argv[3])

if(only_links):
    INPUT_FILE_COLUMN=0

with open(file_links, "r") as f:
    information = csv.reader(f, delimiter='\t')
    # Skip header
    next(information)

    with open(output_file, 'a+') as of:
        csv_writer = csv.writer(of, delimiter='\t')
        data_types = ['id', 'composer', 'work_title', 'keys', 'nb_movements', 'movements', 'instruments']
        csv_writer.writerow(data_types)
        i = 0
        for row in information:
            if(only_links or row[STATUS_COLUMN] in STATUS_OK):
                i = i + 1
                if(i%1 == 1):
                    print(row)
                soup = get_soup_from_link(row[INPUT_FILE_COLUMN])
                get_metadata_from_soup(soup, output_file, csv_writer)

