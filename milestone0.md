# Milestone 0 report

## Research question

We want to study the sameness between several compositions from the same artist, using Vivaldi's concerti as concrete focus.

A lot of well-known composer have been prolific in their work. Sometimes, hearing some music we never heard before we may have the feeling that we know who wrote it. Can a composer's "style" be considered as some sameness between their compositions? And can this sameness be detected by computational means? Vivaldi wrote around 500 concerti for various instruments, mainly for violin, around 1700. Many know this satirical comment attributed to Stravinsky "Vivaldi is overrated, he did not compose 500 concerti but wrote 500 times the same one". Using this satire as an excuse, we want to concentrate on his concerti for this work.

Vivaldi, as a proficient violinist himself, wrote a lot for this instrument. As he wrote for children, he is also one of the composer whose pieces become quickly accessible for young violinist, and a lot of them grow with his pieces as they get better. It is the case of one person in the group, and also a reason to be interested in his work for the others.
Also, we just like Vivaldi a lot.

As the point of our project is to verify Stravinsky's alleged assertion that Vivaldi composed 500 times the same concerto, we may find that the concerti are indeed similar enough to be considered as same, or that they are in fact different concerti, even though the style might feel similar.

## Concepts and data

Note: we are not exactly sure *how* to answer the first 3 questions so we did our best but we might need some help with this part.

+ **What is the concrete focus of your project?**

We will focus on Vivaldi's concerti and trying to assess their similarity.

+ **Which musically relevant concepts do you use or study?**

We looked for computational methods of assessing similarity between music scores. Most of the methods we found were relatively old.

+ **How can they be operationalised?**

We're honestly not sure what this question means.

+ **Datasets**

We want to work mainly with Vivaldi concerto scores (ideally in kern but midi is much more readily available). We thought of possibly use some other scores for comparisons, but it is not a fixed idea yet. Until now, we did not find any datasets per say, but rather some collections of partitions. Some exist in  [kern and midi](http://kern.humdrum.org/search?s=t&keyword=Vivaldi) and some in [midi](https://musescore.com/sheetmusic/artists/vivaldi), but the first regroup a limited set of data, and the second one may prove hard to scrap efficiently and is user-based which could also induce some difficulties (like the question of whether the rating of a score could be considered as a sufficient inducement for the score's quality). We would thus like any suggestion on this part, would it be of other datasets or of scrapping tools.

## Methods

We have found several possible methods (see our Literature section) to compute similarity between pieces but we are not sure which one(s) would be best suited to our project, or which metrics are most relevant. Most of the literature we have found on the subject seems to be targeted at identifying similar songs for recommendation systems (and therefore mostly use recordings rather than music scores?/midi data), so we are not quite sure if they are adapted for the kind of similarity we are trying to identify.

## Literature

+ Shaver-Gleason, Linda, "Did Stravinsky say the Vivaldi wrote “the same concerto, 500 times”? And if so, is it true?", Not Another Music History Cliché!, Blogspot, 28 Oct. 2018, https://notanothermusichistorycliche.blogspot.com/2018/10/did-stravinsky-say-vivaldi-wrote-same.html (last accessed 29/03/2021): this blog post cites interesting sources we will likely not have time to read but was an interesting take on the subject.
+ J. P. Bello, "Measuring Structural Similarity in Music," in IEEE Transactions on Audio, Speech, and Language Processing, vol. 19, no. 7, pp. 2013-2025, Sept. 2011, doi: 10.1109/TASL.2011.2108287. https://doi.org/10.1109/TASL.2011.2108287
+ Berenzweig, Adam, et al. "A large-scale evaluation of acoustic and subjective music-similarity measures." Computer Music Journal 28.2 (2004): 63-76. https://www.jstor.org/stable/3681827
+ Alexander, Peter J. "Entropy and popular culture: product diversity in the popular music recording industry." American Sociological Review 61.1 (1996): 171-174.APA https://doi.org/10.2307/2096412

We have found very little in terms of literature on the supposed sameness of Vivaldi concertos, and none which addresses the issue using computational methods so in this respect our project could add to the existing literature.

## Support

+ Data:
    + Do you have any leads on other datasets/sources we could use? Maybe slightly more reliable ones.
    + How much data should we collect? We will be limited by both space and computational power if our dataset is too large, and given the nature of the data sources we found we are not sure just how much data we are going to get so should we aim for a subset of Vivaldi's concerti (to be honest, we are not even sure if we can find all of them, or even if they all exist in a digitalised version in the public domain)? If so, what resources could help us decide how many and which ones to keep?
+ As we mentioned, we are not sure how to answer the "Concepts"-related questions. Moreover, we might need guidance to choose appropriate methods and metrics for our goal.
+ Somewhat related to our data questions, but we were wondering if you had access to computational resources (like the iccluster we used in our Foundations of Digital Humanities course) on which to store our data and run heavy computations for the duration of the project, as our machines might not be suited to such tasks.
