# import the required libraries
import requests
from bs4 import BeautifulSoup
import sys
import csv
import re
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
from platform import system


VIVALDI_LINK = "https://imslp.org/wiki/Category:Vivaldi%2C_Antonio"
TELEMANN_LINK = "https://imslp.org/wiki/Category:Telemann%2C_Georg_Philipp"
BACH_LINK = "https://imslp.org/wiki/Category:Bach%2C_Johann_Sebastian"
CORELLI_LINK = "https://imslp.org/wiki/Category:Corelli%2C_Arcangelo"

FIREFOX_REGEX = re.compile(r'[Ff][irefox]?')
CHROME_REGEX = re.compile(r'[Cc][hrome]?')

VIVALDI_REGEX = re.compile(r'oncerto[^s]|onata[^s]|infonia[^s]')
OTHERS_REGEX = re.compile(r'on[cz]ert[o][^es]')

def get_link_from_soup(soup, output_file, csv_writer, regex):
   if(soup is not None):
      #td = soup.find_elements_by_xpath('/html/body/div[4]/div/div[1]/div[1]/div[3]/div[1]/div/div/div[2]/table/tbody/tr')
      #for td in soup.find_element_by_class_name("fcatcol"):
      #   print("hey")
      #for a in td.find_element_by_tag_name("a"):
      #print(td)

      #print(td[0].text)
      for td in soup.find_all('td', class_="fcatcol"):
         for a in td.find_all('a', href=True):
              piece_link = "https://imslp.org" + a['href']
              if(regex.search(piece_link)):
                 information_row = [piece_link]
                 csv_writer.writerow(information_row)
              #else:
              #   print("No match for: "+piece_link)
   else:
      print("invalid link")

def get_link_from_page(page_source, output_file, csv_writer, regex=OTHERS_REGEX):
   soup = BeautifulSoup(page_source, "lxml")
   get_link_from_soup(soup, output_file, csv_writer, regex)

def get_soup_from_link(link, output_file, csv_writer, browser, regex=OTHERS_REGEX):
   options = Options()
   options.add_argument("--headless")
   if(FIREFOX_REGEX.match(browser)):
      web_browser = webdriver.Firefox
   elif(CHROME_REGEX.match(browser)):
      web_browser = webdriver.Chrome
   else:
      print(f"Invalid browser: {browser}")
      return
   current_os = system()
   executable_path = "./geckodrivers/" + current_os + "_geckodriver"
   if(current_os=='Windows'):
      executable_path += '.exe'
   driver = web_browser(options=options, executable_path=executable_path)
   driver.get(link)

   # write down links from first page
   get_link_from_page(driver.page_source, output_file, csv_writer, regex=regex)

   next_regex = re.compile(r'next')
   navigation_buttons = list(filter(lambda x: next_regex.search(x.text) , driver.find_elements_by_class_name('categorypaginglink')))
   while(len(navigation_buttons) >= 1):
      print(navigation_buttons[0])
      print(navigation_buttons[0].text)
      navigation_buttons[0].click()
      get_link_from_page(driver.page_source, output_file, csv_writer, regex=regex)
      navigation_buttons = list(filter(lambda x: next_regex.search(x.text) , driver.find_elements_by_class_name('categorypaginglink')))

def write_links_from_link(link, output_file, csv_writer, browser, regex=OTHERS_REGEX):
   print(link)
   get_soup_from_link(link, output_file, csv_writer, browser, regex=regex)

# Main
print(str(sys.argv))

#file_links = sys.argv[1]
output_file = sys.argv[1]
if(len(sys.argv) > 2):
   browser = sys.argv[2]
else:
   browser = 'firefox'

with open(output_file, 'a+') as of:
   csv_writer = csv.writer(of, delimiter='\t')
   column_names = ['link']
   csv_writer.writerow(column_names)
   write_links_from_link(VIVALDI_LINK, output_file, csv_writer, browser, VIVALDI_REGEX)
   write_links_from_link(TELEMANN_LINK, output_file, csv_writer, browser)
   write_links_from_link(BACH_LINK, output_file, csv_writer, browser)
   write_links_from_link(CORELLI_LINK, output_file, csv_writer, browser)

